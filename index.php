<!doctype html>
<html class="no-js" lang="">
	
<head>
  <meta charset="utf-8">
  <title>Test barba home link</title>
  <meta name="description" content="Test barba home link">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui, viewport-fit=cover">
  <link rel="stylesheet" href="/assets/css/main.css">
</head>
<body>

	<header>
		<span><a href="/">Home</a></span>
		<span><a href="/contact">Contact</a></span>
		<span><a href="/about">about</a></span>
	</header>

	<main data-barba="wrapper">

		<?php 
		if( isset( $_GET['page'] ) ) {
			
			$page = $_GET['page'];
			$path = 'pages/'.$page. '.php';

			if ( file_exists($path) ) {
				include $path;
			} else {
				include '404.php';
			}		

		} else {
			include 'pages/home.php';
		}
		?>
	</main>

	<div class="page_curtains">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
	<script src="https://unpkg.com/@barba/router"></script>
	<script src="https://unpkg.com/@barba/core"></script>
  	<script src="/assets/js/app.js"></script>
</body>
</html>