  
  //----------------------------------------------------------------------
  // DEFINE ROUTES
  //----------------------------------------------------------------------
  
  const routes = [
  {
    path: '/:all',
    name: 'default'
  },
//   {
//     path: '/case/:id',
//     name: 'case'
//   },
  ];
  
  // tell Barba to use the router with your custom routes
  barba.use(barbaRouter, {
  routes,
  });
  
  
  
  //----------------------------------------------------------------------
  // TRANSITIONS
  //----------------------------------------------------------------------
  
  barba.init({
  debug: true,
  transitions: [
    {
      name: 'default_anim',
      sync: true,
      to: {
        route: [
          'default', 'home'
        ]
      },
      leave({ current, next, trigger }) {
  
        const page_curtains = document.querySelectorAll('.page_curtains div')
        const done = this.async()
        const tl = new TimelineMax();
  
        tl.set( next.container, 				{ display: 'none' })
        .staggerTo(page_curtains, 		0.5, 	{ y: 0, ease: Power3.easeInOut }, 0.07)
        .set( current.container, 				{ display: 'none', autoAlpha: 0 } )
        .set( next.container, 				 	{ display: 'block' } )
        .staggerTo(  page_curtains, 	0.5, 	{ yPercent: 100, ease: Power3.easeInOut}, 0.07 )
        .set( page_curtains, 					{ clearProps: "all", onComplete : function(){
          clear_clone()
          current.container.remove()
          done()
        } })
  
      }
  
    },
    // {
    //   name: 'to_case_anim',
    //   sync: true,
    //   to: {
    //     route: [
    //       'case',
    //     ]
    //   },
    //   leave({ current, next, trigger }) {
  
    //     const page_curtains = document.querySelectorAll('.page_curtains div')
    //     const done = this.async()
    //     const tl = new TimelineMax();
  
    //     tl.set( next.container, 				{ display: 'none' })
    //     .staggerTo(page_curtains, 		0.5, 	{ y: 0, ease: Power3.easeInOut }, 0.07)
    //     .set( current.container, 				{ display: 'none', autoAlpha: 0 } )
    //     .set( next.container, 				 	{ display: 'block' } )
    //     .to(  page_curtains, 			0.2, 	{ autoAlpha: 0, ease: Power3.easeInOut})
    //     .set( page_curtains, 					{ clearProps: "all", onComplete : function(){
    //       clear_clone()
    //       current.container.remove()          
    //       done()
    //     } })
  
    //   }
  
    // },
  
  ]
  });
  
  
  
  
  //-------------------------------------------------------------------
  // Helper - Clone elm.
  //-------------------------------------------------------------------
  
//   function clone_elm( original ) {
    
//     var clone 		= $(document).find('#clone')
//     var height 		= original.height()
//     var width 		= original.width()
//     var offset_top 	= original.offset().top - $(window).scrollTop()
//     var elm_x 		= original.offset().left
    
//     clone.css({
//         "top": offset_top+'px',
//         "left": elm_x+'px',
//         "width": width,
//         "height": height,
//     })
    
//     clone.html( original.clone() );
//     return clone
    
//   }
  
  function clear_clone() {
    $(document).find('#clone').attr('style', '').html('')
  }
  